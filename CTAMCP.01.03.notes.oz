/*
An example of delaring and using a variable in a calculation
   */
   
declare
V=9999*9999

{Browse V*V}

/*
Definining a recursive function for calculating Factorials
*/
				   
declare
fun {Fact N}
   if N==0 then 1 else N*{Fact N-1} end
end

{Browse {Fact 100}}

/*
A function for computing combinations "n choose k"
	      */

declare
fun {Combo N K}
   {Fact N}
   div
   ({Fact K}*{Fact N-K})
end
