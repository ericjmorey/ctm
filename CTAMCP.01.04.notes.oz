/*
Concepts, Techniques, and Models of Computer Programming
   Section 1.04
*/
   
   /*
   Browse a list
   */
   {Browse [5 6 7 8]}

   /*
   Append a list using variables by browsing a cons.
   */
   
declare
H=5
T=[6 7 8]
{Browse H|T}

/* Demonstrate the dot operators on a list.*/

declare
L=[5 6 7 8]
{Browse L.1}
{Browse L.2}

/* Use a case statement to use pattern matching to declare the head and tail of a list as local variables. Browse the new variables. */

declare
L=[5 6 7 8]
case L of H|T then {Browse H} {Browse T} end
