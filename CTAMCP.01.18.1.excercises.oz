/*
Concepts, Techniques, and Models of Computer Programming
   Exercises 1.18
   */



/*
exercise 1.18.1.A

Calculate the value of 2^(100) without having to type 2*2*2...*2 with 100 2s.
Use variables and define new functions
*/

/*
first with no recursion
*/

declare 
N2=2*2
{Browse N2*N2}

declare
N4=N2*N2
declare
N8=N4*N4
declare
N16=N8*N8
declare
N32=N16*N16
declare
N64=N32*N32
declare
N100=N64*N32*N4

{Browse N100}

/*
now with recursion
*/

declare
fun {Powerpositive X Y}
   if Y == 0 then
      1
   else if Y >= 1 then
	   X * {Powerpositive X (Y-1)}
	end
   end
end

{Browse {Powerp 2 100}}

{Browse 0-1}

/* 
Exercise 1.18.1.B

Calculate the exact value of 100! without using any new functions.

There is no apparent shortcut withou using a new function.
*/

{Browse
 1*2*3*4*5*6*7*8*9*
 10*11*12*13*14*15*16*17*18*19*
 20*21*22*23*24*25*26*27*28*29*
 30*31*32*33*34*35*36*37*38*39*
 40*41*42*43*44*45*46*47*48*49*
 50*51*52*53*54*55*56*57*58*59*
 60*61*62*63*64*65*66*67*68*69*
 70*71*72*73*74*75*76*77*78*79*
 80*81*82*83*84*85*86*87*88*89*
 90*91*92*93*94*95*96*97*98*99*
 100}

/*
Compare to a recursive function
*/

declare
fun {Fact N}
   if N==0 then 1 else N*{Fact N-1} end
end

{Browse {Fact 100}}